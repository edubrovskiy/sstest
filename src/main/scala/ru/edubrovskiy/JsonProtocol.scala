package ru.edubrovskiy

import ru.edubrovskiy.entities._
import spray.json._

object JsonProtocol extends DefaultJsonProtocol {

  implicit val printer = PrettyPrinter

  implicit val questionFormat = jsonFormat(Question, "question_id", "tags", "is_answered")
  implicit val searchResponseFormat = jsonFormat(SearchResponse, "items")
  implicit val tagStatsFormat = jsonFormat2(TagStats)
}
