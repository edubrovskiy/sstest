package ru.edubrovskiy.utils

import akka.http.scaladsl.coding._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.headers.HttpEncodings

object HttpResponseUtils {

  // We're using this method because
  // there is currently no automatic support for decoding responses on the client-side.
  // See https://doc.akka.io/docs/akka-http/current/scala/http/common/encoding.html
  def decode(response: HttpResponse): HttpResponse = {
    val decoder = response.encoding match {
      case HttpEncodings.gzip ⇒ Gzip
      case HttpEncodings.deflate ⇒ Deflate
      case HttpEncodings.identity ⇒ NoCoding
      case _ => ???
    }

    decoder.decodeMessage(response)
  }
}
