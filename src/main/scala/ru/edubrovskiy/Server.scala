package ru.edubrovskiy

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes._
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import JsonProtocol._

import scala.io.StdIn

object Server extends App with LazyLogging {

  val conf = ConfigFactory.load()

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val stackoverflowClient = new StackoverflowClient(conf)

  val route =
    path("") {
      get {
        parameters("tag".as[String].*) { tags =>
          onSuccess(stackoverflowClient.search(tags.toSet)) { questions =>
            val stats = Stats.calcStats(questions)
            complete(stats)
          }
        }
      }
    }

  val host = conf.getString("server.host")
  val port = conf.getInt("server.port")
  val bindingFuture = Http().bindAndHandle(route, host, port)

  println(s"Server online at http://$host:$port/\nPress RETURN to stop...")
  StdIn.readLine()

  bindingFuture.flatMap(_.unbind)
    .flatMap { _ =>
      Http().shutdownAllConnectionPools()
    }.flatMap { _ =>
      system.terminate()
    }
}
