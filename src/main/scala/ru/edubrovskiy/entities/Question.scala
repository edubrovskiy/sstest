package ru.edubrovskiy.entities

case class Question(id: Long, tags: Set[String], answered: Boolean)
