package ru.edubrovskiy.entities

case class TagStats(total: Int, answered: Int)
