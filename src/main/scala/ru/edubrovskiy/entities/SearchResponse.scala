package ru.edubrovskiy.entities

case class SearchResponse(questions: Seq[Question])
