package ru.edubrovskiy

import com.typesafe.scalalogging.LazyLogging
import ru.edubrovskiy.entities.{Question, TagStats}

object Stats extends LazyLogging {

  def calcStats(questions: Set[Question]): Map[String, TagStats] = {
    questions.flatMap(_.tags).map { tag =>
      val total = questions.count(_.tags.contains(tag))
      val answered = questions.count(question => question.tags.contains(tag) && question.answered)
      tag -> TagStats(total, answered)
    }.toMap
  }
}
