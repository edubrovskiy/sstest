package ru.edubrovskiy

import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, Uri}
import akka.http.scaladsl.unmarshalling.Unmarshal
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import ru.edubrovskiy.entities.{Question, SearchResponse}
import JsonProtocol._
import akka.actor.ActorSystem
import akka.stream.Materializer
import ru.edubrovskiy.utils.HttpResponseUtils
import scala.concurrent.{Future, Promise}
import scala.collection.mutable

class StackoverflowClient(conf: Config)
                         (implicit val actorSystem: ActorSystem, materializer: Materializer) extends LazyLogging {

  private implicit val executionContext = actorSystem.dispatcher

  private val searchUri = Uri(conf.getString("stackoverflowClient.searchUri"))
  private val defaultParams = Map(
    "pagesize" -> "100",
    "order" -> "desc",
    "sort" -> "creation",
    "site" -> "stackoverflow"
  )

  def search(tags: Set[String]): Future[Set[Question]] = {
    def search(tag: String): Future[SearchResponse] = {
      val query = Query(defaultParams + ("tagged" -> tag))
      val request = HttpRequest(uri = searchUri.withQuery(query))
      makeRequest(request).transform { response =>
        releaseConnection()
        response
      }.flatMap { response =>
        Unmarshal(HttpResponseUtils.decode(response)).to[SearchResponse]
      }
    }

    Future.sequence(tags.map(search)).map { responses =>
      responses.flatMap(_.questions)
    }
  }

  private val maxConnections = conf.getInt("stackoverflowClient.maxConnections")
  require(maxConnections > 0)

  private val queue = mutable.Queue[(HttpRequest, Promise[HttpResponse])]()
  private var connectionCount = 0

  private def makeRequest(request: HttpRequest): Future[HttpResponse] = synchronized {
    if (connectionCount == maxConnections) {
      val promise = Promise[HttpResponse]()
      queue += (request -> promise)
      promise.future
    } else {
      connectionCount += 1
      doMakeRequest(request)
    }
  }

  private def releaseConnection(): Unit = synchronized {
    require(connectionCount > 0)
    if (queue.isEmpty) {
      connectionCount -= 1
    } else {
      val (request, promise) = queue.dequeue()
      promise.completeWith(doMakeRequest(request))
    }
  }

  private def doMakeRequest(request: HttpRequest): Future[HttpResponse] = {
    logger.debug(s"making request $request")
    Http().singleRequest(request)
  }
}
